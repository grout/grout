/*
 * Copyright 2019 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.grout;

import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static com.gitlab.grout.Segment.segment;
import static com.gitlab.grout.TestHelpers.aString;
import static com.gitlab.grout.TestHelpers.aStringNotEqualTo;
import static net.sourceforge.urin.Path.path;
import static net.sourceforge.urin.scheme.http.Http.HTTP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class GroutTest {

    @Test
    void handlesMultipleSegments() throws Exception {
        final var value = new Object();
        final var grout = Grout.path("foo", "bar").resolvesTo(value); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo%2Fbar"), aString()), equalTo(Optional.empty())); // NOPMD
    }

    @Test
    void handlesMultipleSegmentsViaStringGrout() throws Exception {
        final var value = new Object();
        final var grout = Grout.path("foo", "bar").resolvesTo(value); // NOPMD
        assertThat(grout.grout("/foo/bar", aString()), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout("/foo%2Fbar", aString()), equalTo(Optional.empty())); // NOPMD
    }

    @Test
    void rejectsInvalidUri() {
        final var value = new Object();
        final var grout = Grout.path("foo", "bar").resolvesTo(value); // NOPMD
        assertThrows(InvalidUriException.class, () -> grout.grout("not a valid URI", aString()));
    }

    @Test
    void handlesMultiplePaths() throws Exception {
        final var fooValue = new Object();
        final var barValue = new Object();
        final var grout = Grout.path("foo").resolvesTo(fooValue).otherwise(
                Grout.path("bar").resolvesTo(barValue));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(fooValue))); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.of(barValue))); // NOPMD
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesNoPathsSpecifiedThroughList() throws Exception {
        final var grout = Grout.grout(List.of());
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty())); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesSinglePathSpecifiedThroughList() throws Exception {
        final var value = new Object();
        final var grout = Grout.grout(List.of(
                Grout.path("foo").resolvesTo(value)
        ));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(value))); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesMultiplePathsSpecifiedThroughList() throws Exception {
        final var fooValue = new Object();
        final var barValue = new Object();
        final var grout = Grout.grout(List.of(
                Grout.path("foo").resolvesTo(fooValue),
                Grout.path("bar").resolvesTo(barValue)
        ));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(fooValue))); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.of(barValue)));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesMultiplePathsSpecifiedThroughListInCorrectOrder() throws Exception {
        final var fooValue = new Object();
        final var grout = Grout.grout(List.of(
                Grout.path("foo").resolvesTo(fooValue),
                Grout.path("foo").resolvesTo(new Object())
        ));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(fooValue))); //NOPMD
    }

    @Test
    void handlesNoPathsSpecifiedThroughVarargs() throws Exception {
        final var grout = Grout.grout();
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty())); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesSinglePathSpecifiedThroughVarargs() throws Exception {
        final var value = new Object();
        final var grout = Grout.grout(Grout.path("foo").resolvesTo(value));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(value))); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesMultiplePathsSpecifiedThroughVarargs() throws Exception {
        final var fooValue = new Object();
        final var barValue = new Object();
        final var grout = Grout.grout(
                Grout.path("foo").resolvesTo(fooValue),
                Grout.path("bar").resolvesTo(barValue)
        );
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(fooValue))); //NOPMD
        assertThat(grout.grout(URI.create("/bar"), aString()), equalTo(Optional.of(barValue)));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aStringNotEqualTo("foo", "bar")))).asUri(), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(HTTP.relativeReference((path(aString(), aString()))).asUri(), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesMultiplePathsSpecifiedThroughVarargsInCorrectOrder() throws Exception {
        final var fooValue = new Object();
        final var grout = Grout.grout(
                Grout.path("foo").resolvesTo(fooValue),
                Grout.path("foo").resolvesTo(new Object())
        );
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(fooValue))); //NOPMD
    }

    @Test
    void handlesSegmentContainingSlash() throws Exception {
        final var value = new Object();
        final var grout = Grout.path("foo/bar").resolvesTo(value);
        assertThat(grout.grout(URI.create("/foo%2Fbar"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.empty()));
    }

    @Test
    void handlesTrailingSlash() throws Exception {
        final var value = new Object();
        final var grout = Grout.path("foo").resolvesTo(value);
        assertThat(grout.grout(URI.create("/foo/"), aString()), equalTo(Optional.empty()));
    }

    @Test
    void canPassThroughSegments() throws Exception {
        final var resolution = new Object();
        final var grout = Grout.path("foo", "bar").resolvesWith(segments -> resolution);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(resolution)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty())); // NOPMD sigh
    }

    @Test
    void canPassThroughSegmentsAndMethod() throws Exception {
        final var resolution = new Object();
        final var grout = Grout.path("foo", "bar").resolvesWith((segments, method) -> resolution);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(resolution)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty())); // NOPMD sigh
    }

    @Test
    void canMatchSegmentsAnotherWay() throws Exception {
        final var resolution = new Object();
        final var grout = Grout.path(segment("foo"), segment("bar")).resolvesTo(resolution);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(resolution)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty())); // NOPMD sigh
    }

    @Test
    void canPassThroughSegmentsAnotherWay() throws Exception {
        final var resolution = new Object();
        final var grout = Grout.path(segment("foo"), segment("bar")).resolvesWith(segments -> resolution);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(resolution)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty()));
    }

    @Test
    void canPassThroughSegmentsAndMethodAnotherWay() throws Exception {
        final var resolution = new Object();
        final var grout = Grout.path(segment("foo"), segment("bar")).resolvesWith((segments, method) -> resolution);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(resolution)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty()));
    }

    @Test
    void canMatchAllPaths() throws Exception {
        final var value = new Object();
        final var grout = Grout.any().resolvesTo(value);
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.of(value)));
    }

    @Test
    void canAcceptMatchInResolution() throws Exception {
        final var value = new Object();
        final var grout = Grout.any().mayResolveWith(segments -> Optional.of(value));
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.of(value)));
    }

    @Test
    void canRejectMatchInResolution() throws Exception {
        final var grout = Grout.any().mayResolveWith(segments -> Optional.empty());
        assertThat(grout.grout(URI.create("/foo/bar"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo/bar/"), aString()), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/"), aString()), equalTo(Optional.empty()));
    }

    @Test
    void canDetermineMatchInResolutionBasedOnMethod() throws Exception {
        final var value = new Object();
        final var matchingMethod = aString();
        final var grout = Grout.any().mayResolveWith((segments, method) -> matchingMethod.equals(method) ? Optional.of(value) : Optional.empty());
        assertThat(grout.grout(URI.create("/foo/bar"), matchingMethod), equalTo(Optional.of(value)));
        assertThat(grout.grout(URI.create("/foo"), aStringNotEqualTo(matchingMethod)), equalTo(Optional.empty()));
    }

    @Test
    void canMatchOnMethod() throws Exception {
        final var value = new Object();
        final var method = aString();
        final var grout = Grout.methods(method).resolvesTo(value); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), method), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aStringNotEqualTo(method)), equalTo(Optional.empty()));
    }

    @Test
    void canMatchOnMultipleMethods() throws Exception {
        final var value = new Object();
        final var method = aString();
        final var anotherMethod = aString();
        final var grout = Grout.methods(method, anotherMethod).resolvesTo(value); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), method), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), anotherMethod), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aStringNotEqualTo(method, anotherMethod)), equalTo(Optional.empty()));
    }

    @Test
    void canMatchOnPathAndThenMethod() throws Exception {
        final var value = new Object();
        final var method = aString();
        final var grout = Grout.path("foo", "bar").then(
                Grout.methods(method).resolvesTo(value)
        ); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), method), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aStringNotEqualTo(method)), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), method), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), aStringNotEqualTo(method)), equalTo(Optional.empty()));
    }

    @Test
    void canMatchOnPathAndThenMultipleMethodsThroughList() throws Exception {
        final var value = new Object();
        final var anotherValue = new Object();
        final var method = aString();
        final var anotherMethod = aString();
        final var grout = Grout.path("foo", "bar").then(List.of(
                Grout.methods(method).resolvesTo(value),
                Grout.methods(anotherMethod).resolvesTo(anotherValue)
        )); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), method), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), anotherMethod), equalTo(Optional.of(anotherValue))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aStringNotEqualTo(method, anotherMethod)), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), method), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), aStringNotEqualTo(method, anotherMethod)), equalTo(Optional.empty()));
    }

    @Test
    void canMatchOnPathAndThenMultipleMethodsThroughVarargs() throws Exception {
        final var value = new Object();
        final var anotherValue = new Object();
        final var method = aString();
        final var anotherMethod = aString();
        final var grout = Grout.path("foo", "bar").then(
                Grout.methods(method).resolvesTo(value),
                Grout.methods(anotherMethod).resolvesTo(anotherValue)
        ); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), method), equalTo(Optional.of(value))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), anotherMethod), equalTo(Optional.of(anotherValue))); // NOPMD
        assertThat(grout.grout(URI.create("/foo/bar"), aStringNotEqualTo(method, anotherMethod)), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), method), equalTo(Optional.empty()));
        assertThat(grout.grout(URI.create("/foo%2Fbar"), aStringNotEqualTo(method, anotherMethod)), equalTo(Optional.empty()));
    }

}
