package com.gitlab.grout;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;
import java.util.Set;

public final class TestHelpers {

    private static final Random RANDOM = new Random();

    public static String aString() {
        return RandomStringUtils.random(RANDOM.nextInt(10));
    }

    public static String aStringNotEqualTo(final String... exclusions) {
        final var excludedSet = Set.of(exclusions);
        String candidate;
        do {
            candidate = aString();
        } while (excludedSet.contains(candidate));
        return candidate;
    }
}
