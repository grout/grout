/*
 * Copyright 2019 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.grout;

import org.junit.jupiter.api.Test;

import static com.gitlab.grout.TestHelpers.aString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SegmentTest {

    @Test
    void emptySegmentHasNoValue() {
        assertThat(Segment.empty().hasValue(), equalTo(false));
    }

    @Test
    void gettingValueForEmptySegmentThrowsUnsupportedOperationException() {
        assertThrows(UnsupportedOperationException.class, () -> Segment.empty().value());
    }

    @Test
    void emptySegmentToStringIsCorrect() {
        assertThat(Segment.empty().toString(), equalTo("EmptySegment{}"));
    }

    @Test
    void regularSegmentRejectsNullValue() {
        assertThrows(NullPointerException.class, () -> Segment.segment(null));
    }

    @Test
    void regularSegmentHasValue() {
        assertThat(Segment.segment(aString()).hasValue(), equalTo(true));
    }

    @Test
    void gettingValueForRegularSegmentReturnsTheValue() {
        final var value = aString();
        assertThat(Segment.segment(value).value(), equalTo(value));
    }

    @Test
    void regularSegmentToStringIsCorrect() {
        final var value = aString();
        assertThat(Segment.segment(value).toString(), equalTo("RegularSegment{value='" + value + "'}"));
    }

    @Test
    void emptySegmentEqualsAnotherEmptySegment() {
        assertThat(Segment.empty(), equalTo(Segment.empty()));
        assertThat(Segment.empty().hashCode(), equalTo(Segment.empty().hashCode()));
    }

    @Test
    void regularSegmentEqualsAnotherRegularSegmentWithTheSameValue() {
        final var value = aString();
        assertThat(Segment.segment(value), equalTo(Segment.segment(value)));
        assertThat(Segment.segment(value).hashCode(), equalTo(Segment.segment(value).hashCode()));
    }

    @Test
    void regularSegmentDoesNotEqualAnotherRegularSegmentWithADifferentValue() {
        final var value = aString();
        final String differentValue = TestHelpers.aStringNotEqualTo(value);
        assertThat(Segment.segment(value), not(equalTo(Segment.segment(differentValue))));
    }

    @Test
    void regularSegmentDoesNotEqualEmptySegment() {
        assertThat(Segment.segment(aString()), not(equalTo(Segment.empty())));
    }

}