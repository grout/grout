/*
 * Copyright 2019 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.grout;

import java.util.Objects;

public abstract class Segment {

    public static Segment empty() {
        return EmptySegment.EMPTY_SEGMENT;
    }

    public static Segment segment(final String value) {
        return new RegularSegment(value);
    }

    public abstract boolean hasValue();

    public abstract String value() throws UnsupportedOperationException;

    private static final class EmptySegment extends Segment {

        static final Segment EMPTY_SEGMENT = new EmptySegment();

        @Override
        public boolean hasValue() {
            return false;
        }

        @Override
        public String value() throws UnsupportedOperationException {
            throw new UnsupportedOperationException("Empty segment doesn't have a value");
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            return o != null && getClass() == o.getClass();
        }

        @Override
        public int hashCode() {
            return 42;
        }

        @Override
        public String toString() {
            return "EmptySegment{}";
        }
    }

    private static final class RegularSegment extends Segment {

        private final String value; //NOPMD

        RegularSegment(final String value) {
            this.value = Objects.requireNonNull(value, "Value cannot be null");
        }

        @Override
        public boolean hasValue() {
            return true;
        }

        @Override
        public String value() throws UnsupportedOperationException {
            return value;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final var that = (RegularSegment) o;
            return value.equals(that.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return "RegularSegment{" +
                    "value='" + value + '\'' +
                    '}';
        }
    }
}
