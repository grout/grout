/*
 * Copyright 2019 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.grout;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public abstract class Match {
    public final <T> Grout<T> resolvesTo(T resolution) {
        return resolvesWith(ignored -> resolution);
    }

    public final <T> Grout<T> resolvesWith(SegmentsResolver<T> resolver) {
        return mayResolveWith(segments -> Optional.of(resolver.resolve(segments)));
    }

    public final <T> Grout<T> resolvesWith(SegmentsAndMethodResolver<T> resolver) {
        return mayResolveWith((segments, method) -> Optional.of(resolver.resolve(segments, method)));
    }

    public final <T> Grout<T> mayResolveWith(SegmentsResolver<Optional<T>> resolver) {
        return mayResolveWith((segments, ignored) -> resolver.resolve(segments));
    }

    public final <T> Grout<T> mayResolveWith(SegmentsAndMethodResolver<Optional<T>> resolver) {
        return new Grout<>() {
            @Override
            Optional<T> grout(List<Segment> segments, String method) {
                if (matches(segments, method)) {
                    return resolver.resolve(segments, method);
                } else {
                    return Optional.empty();
                }
            }
        };
    }

    abstract boolean matches(List<Segment> segments, String method);

    @SafeVarargs
    public final <T> Grout<T> then(Grout<T>... grouts) {
        return then(Arrays.asList(grouts));
    }

    public final <T> Grout<T> then(List<Grout<T>> grouts) {
        return then(Grout.grout(grouts));
    }

    public final <T> Grout<T> then(Grout<T> grout) {
        return new Grout<>() {
            @Override
            Optional<T> grout(List<Segment> segments, String method) {
                if (matches(segments, method)) {
                    return grout.grout(segments, method);
                } else {
                    return Optional.empty();
                }
            }
        };
    }
}
