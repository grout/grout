/*
 * Copyright 2019 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.grout;

import net.sourceforge.urin.ParseException;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static net.sourceforge.urin.scheme.http.Http.HTTP;

public abstract class Grout<T> {

    Grout() {
    }

    private static <T> Grout<T> identity() {
        return new Grout<>() {
            @Override
            Optional<T> grout(final List<Segment> segments, final String method) {
                return Optional.empty();
            }
        };
    }

    @SafeVarargs
    public static <T> Grout<T> grout(Grout<T>... grouts) {
        return grout(Arrays.asList(grouts));
    }

    public static <T> Grout<T> grout(List<Grout<T>> grouts) {
        return grouts.stream().reduce(identity(), Grout::otherwise);
    }

    public static Match methods(final String... otherMethods) {
        final var methods = Set.copyOf(asList(otherMethods)); // NOPMD Bizarre
        return new Match() {
            @Override
            boolean matches(List<Segment> segments, String method) {
                return methods.contains(method);
            }
        };
    }

    public static Match path(final String... segments) {
        return path(Arrays.stream(segments).map(com.gitlab.grout.Segment::segment).collect(toList()));
    }

    public static Match path(final com.gitlab.grout.Segment... segments) {
        return path(List.of(segments));
    }

    public static Match path(List<com.gitlab.grout.Segment> segments) {
        final var immutableExpected = List.copyOf(segments); // NOPMD Bizarre
        return new Match() {
            @Override
            boolean matches(List<Segment> segments, String method) {
                return immutableExpected.equals(segments);
            }
        };
    }

    public static Match any() {
        return new Match() {
            @Override
            boolean matches(List<Segment> segments, String method) {
                return true;
            }
        };
    }

    public final Optional<T> grout(final URI uri, final String method) throws InvalidUriException {
        try {
            final var segments = HTTP.parseUrinReference(uri).path().segments().stream().map(segment -> segment.hasValue() ? com.gitlab.grout.Segment.segment(segment.value()) : com.gitlab.grout.Segment.empty()).collect(toList());
            return grout(segments, method);
        } catch (final ParseException e) {
            throw new InvalidUriException("Cannot parse URI: " + uri);
        }
    }

    public final Optional<T> grout(final String uri, final String method) throws InvalidUriException {
        try {
            final var segments = HTTP.parseUrinReference(uri).path().segments().stream().map(segment -> segment.hasValue() ? com.gitlab.grout.Segment.segment(segment.value()) : com.gitlab.grout.Segment.empty()).collect(toList());
            return grout(segments, method);
        } catch (ParseException e) {
            throw new InvalidUriException("Cannot parse URI: " + uri);
        }
    }

    abstract Optional<T> grout(List<Segment> segments, String method);

    public final Grout<T> otherwise(final Grout<T> alternative) {
        return new Grout<>() {
            @Override
            Optional<T> grout(final List<Segment> segments, final String method) {
                final Optional<T> resolution = Grout.this.grout(segments, method);
                return resolution.isPresent() ? resolution : alternative.grout(segments, method);
            }
        };
    }
}
